import React from 'react';

import "./header.css";

const Header: React.FC = () => {
    return (
        <div className="header">
            <h1>Pricing</h1>
            <p>Quickly build an effective pricing table for your potential customers with this layout. It's built with default Material-UI components with little customization.</p>
        </div>
    );
}

export default Header;