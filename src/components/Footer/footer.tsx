import React from 'react';

import {Footer} from '../../types';

import './footer.css';

interface Props {
    footer: Footer;
}

const FooterItem: React.FC<Props> = ({footer}) => {
    return (
        <div className="footer">        
            <h3>{footer.title}</h3>
            {footer.items.map(ele => {
                return <p>{ele}</p>
            })}
        </div>
    );
}

export default FooterItem;