import React from 'react';

import {Card} from '../../types';

import './card.css';

interface Props {
    card: Card;
    selected: boolean;
    clicked(): void;
}

const CardItem: React.FC<Props> = ({card, selected, clicked}) => {
    return (
        <div className="card" onClick={clicked} style={selected ? {height: "450px"}: undefined}>
            <h1>{card.title}</h1>
            <p>${card.price}/mo</p>
            <div>
                {card.content.map(cont => {
                    return <p>{cont}</p>
                })}
            </div>
            <button>{card.buttonText}</button>
        </div>
    );
}

export default CardItem;