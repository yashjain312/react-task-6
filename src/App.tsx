import React from 'react';

import Headers from './components/Header/header';
import CardItem from './components/Card/card';
import FooterItem from './components/Footer/footer';

import {Card} from './types';
import {Footer} from './types';

import './App.css';

interface State {
  selected: number
}

interface Props {}

const footerData: Array<Footer> = [
  {
    title: "Company",
    items: [
      "Team",
      "History",
      "Contact Us",
      "Locations"
    ]
  },
  {
    title: "Features",
    items: [
      "Cool stuff",
      "Random feature",
      "Team feature",
      "Developer stuff",
      "Another one"
    ]
  },
  {
    title: "Resources",
    items: [
      "Resource",
      "Resource name",
      "Another resource",
      "Final resource"
    ]
  },
  {
    title: "Legal",
    items: [
      "Privacy policy",
      "Terms of use"
    ]
  }
]

const cardData: Array<Card> = [
  {
    title: "Free",
    price: 0,
    content: [
      "10 users included",
      "2GB of storage",
      "Help center access",
      "Email support"
    ],
    buttonText: "Sign up for free",
  },
  {
    title: "Pro",
    price: 15,
    content: [
      "20 users included",
      "10GB of storage",
      "Help center access",
      "Priority email support"
    ],
    buttonText: "Get started",
  },
  {
    title: "Enterprise",
    price: 30,
    content: [
      "50 users included",
      "30GB of storage",
      "Help center access",
      "Phone and Email support"
    ],
    buttonText: "Contact us",
  }
]


class App extends React.Component<Props, State>  {
  constructor(props: Props) {
    super(props);
    this.state = {
      selected: 1
    }
  }

  selectedCard = (newSelected: number) => {
    this.setState({
      selected: newSelected
    })
  }

  render() {
    return (
      <div className="App">
        <Headers/>
  
        <div className="card-section"> 
          {cardData.map((card, index) => {
            return <CardItem card={card} selected={index == this.state.selected} clicked={() => this.selectedCard(index)}/> 
          })}
        </div>
  
        <hr/>
  
        <div className="footer-section">
          {footerData.map((data) => {
            return <FooterItem footer={data}/>
          })}
        </div>
      </div>
    );
  }
}

export default App;
