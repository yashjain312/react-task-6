export interface Card {
    title: String,
    price: Number,
    content: Array<String>,
    buttonText: String,
    selected: Boolean
}

export interface Footer {
    title: String,
    items: Array<String>
}
  